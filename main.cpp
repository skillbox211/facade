#include "game_components.h"
#include <fstream>


/////////////////////////////////////////////////////////////////////////////////////////
//// VikBo - pattern Singleton
/////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////
//// Configuration Struct
/////////////////////////////////////////////////////////////////////////////////////////
struct ActorConfig_Start
{
    GameVector ActorLocation{};
    GameVector ActorRotation{};
    float Opacity{};
};


/////////////////////////////////////////////////////////////////////////////////////////
//// parsing of ".txt" file
/////////////////////////////////////////////////////////////////////////////////////////
class WorkWithConfigFile
{
public:
    WorkWithConfigFile()= default;;
    ~WorkWithConfigFile() = default;

    static std::vector<std::string> MySplit(const std::string &text, char sep) {
        std::vector<std::string> tokens;
        std::size_t start = 0, end = 0;
        while ((end = text.find(sep, start)) != std::string::npos) {
            tokens.push_back(text.substr(start, end - start));
            start = end + 1;
        }
        tokens.push_back(text.substr(start));
        return tokens;
    }

    static void ReloadConfigDataFromFiles(ActorConfig_Start* ActorConf, const std::string& filename)
    {
        std::string CurrentPath = std::filesystem::current_path();
        std::fstream newfile;

        newfile.open(CurrentPath + filename,std::ios::in);  // open a file to perform write operation using file object

        if (newfile.is_open()){ //checking whether the file is open
            std::string tp;
            while(getline(newfile, tp)){ //read data from file object and put it into string.
                auto FirstSplit = MySplit(tp, '=');
                auto SecondSplit = MySplit(FirstSplit[1], ';');
                if (FirstSplit[0] == "ActorLocation")
                {
                    ActorConf->ActorLocation.SetCoordinates(std::stof(SecondSplit[0]),
                                                            std::stof(SecondSplit[1]),
                                                            std::stof(SecondSplit[2]));
                } else if (FirstSplit[0] == "ActorRotation")
                {
                    ActorConf->ActorRotation.SetCoordinates(std::stof(SecondSplit[0]),
                                                            std::stof(SecondSplit[1]),
                                                            std::stof(SecondSplit[2]));
                } else if (FirstSplit[0] == "Opacity")
                {
                    ActorConf->Opacity = std::stof(SecondSplit[0]);
                }

            }
            newfile.close(); //close the file object.
        }
    }

};

/////////////////////////////////////////////////////////////////////////////////////////
//// Proxy for work with parsing ".txt" file
/////////////////////////////////////////////////////////////////////////////////////////
class ProxyConfig
{
protected:
    std::string FileName;

public:
    explicit ProxyConfig(std::string CurrenName){
        FileName = std::move(CurrenName);
    }
    ~ProxyConfig() = default;

    void UpdateInfo()
    {
        /* this can be written logic after reading data from config file */
        WorkWithConfigFile::ReloadConfigDataFromFiles(&ActorConf, FileName);
        /* this can be written logic before reading data from config file */
    }

    ActorConfig_Start ActorConf = ActorConfig_Start();
};





/////////////////////////////////////////////////////////////////////////////////////////
//// Game Facade
/////////////////////////////////////////////////////////////////////////////////////////
class Game
{
protected:
    ProxyConfig PlayerProxyConf = ProxyConfig("/player_config.txt");
    ProxyConfig EnemyProxyConf = ProxyConfig("/enemy_config.txt");


    std::shared_ptr<Enemy> GameEnemyActor;

public:

    std::shared_ptr<Player> GamePlayerActor;

    explicit Game()
    {

        PlayerProxyConf.UpdateInfo();
        EnemyProxyConf.UpdateInfo();

        Actor* PointToPlayer = ActorFactory::NewActor(FActorClass::Player,
                                                      PlayerProxyConf.ActorConf.ActorLocation,
                                                      PlayerProxyConf.ActorConf.ActorRotation,
                                                      PlayerProxyConf.ActorConf.Opacity);

        GamePlayerActor = static_cast<std::shared_ptr<Player>>((Player*)(PointToPlayer));

        Actor* PointToEnemy = ActorFactory::NewActor(FActorClass::Enemy,
                                                     EnemyProxyConf.ActorConf.ActorLocation,
                                                     EnemyProxyConf.ActorConf.ActorRotation,
                                                     EnemyProxyConf.ActorConf.Opacity);

        GameEnemyActor = static_cast<std::shared_ptr<Enemy>>((Enemy*)(PointToEnemy));

    };
    ~Game() = default;

    bool GetVisibilityBetweenActors(){
        bool IsVisible = true;
        if (GamePlayerActor->GetActorOpacity() < 0.5f || GameEnemyActor->GetActorOpacity() < 0.5f)
            IsVisible = false;
        return IsVisible;
    };

    float GetLocationVectorLengthBetweenActors(){
        GameVector DifferenceVector = GamePlayerActor->GetActorLocation() - GameEnemyActor->GetActorLocation();
        return DifferenceVector.GetLength();
    };

    GameVector GetRotationVectorBetweenActors(){
        GameVector DifferenceVector = GamePlayerActor->GetActorRotation() - GameEnemyActor->GetActorRotation();
        return DifferenceVector;
    };

    std::shared_ptr<Player> GetPlayerActor()
    {
        return GamePlayerActor;
    }

    std::shared_ptr<Enemy> GetEnemyActor()
    {
        return GameEnemyActor;
    }

};


int main(int argc, char * argv[]) {

    Game MyGame = Game();

    if (MyGame.GetVisibilityBetweenActors())
    {
        std::cout << "Player and Enemy see each other" << std::endl;
        std::cout << "Vector length of difference between player and enemy Locations - " << MyGame.GetLocationVectorLengthBetweenActors() << std::endl;
        std::cout << "Vector of difference between player and enemy rotations - " << MyGame.GetRotationVectorBetweenActors() << std::endl << std::endl;
    }
    else
        std::cout << "Player and Enemy does not see each other" << std::endl << std::endl;


    auto MyPlayer = MyGame.GetPlayerActor();
    auto MyEnemy = MyGame.GetEnemyActor();

    MyPlayer->PlayerBag.AppendNewSlot(new Shootgun());
    MyPlayer->PlayerBag.AppendNewSlot(new Spoon());
    MyPlayer->PlayerBag.AppendNewSlot(new Apple());
    std::cout << "Full weight of Player's wearing items - " << MyPlayer->PlayerBag.GetWeight() << std::endl;
    std::cout << "Full price of Player's wearing items - " << MyPlayer->PlayerBag.GetPrice() << std::endl << std::endl;

    Shootgun* MyShootgun = dynamic_cast<Shootgun*>(MyPlayer->PlayerBag.GetSlots()[0]);
    if (MyShootgun)
    {
        std::cout << "Player fired from shotgun on the enemy!" << std::endl;
        std::cout << "The enemy has left " << MyEnemy->ActorHealth.ChangeHealth(MyShootgun) << "HP" << std::endl;
    }

    Spoon* MySpoon = dynamic_cast<Spoon*>(MyPlayer->PlayerBag.GetSlots()[1]);
    if (MySpoon)
    {
        Weapon SpoonWeapon = WearableItemsLikeWeapon::GetNewWeapon(MyPlayer.get(), MySpoon);
        std::cout << "Player thrown spoon at the enemy!" << std::endl;
        std::cout << "The enemy has left " << MyEnemy->ActorHealth.ChangeHealth(&SpoonWeapon) << "HP" << std::endl;
    }

    Apple* MyApple = dynamic_cast<Apple*>(MyPlayer->PlayerBag.GetSlots()[2]);
    if (MySpoon)
    {
        Weapon AppleWeapon = WearableItemsLikeWeapon::GetNewWeapon(MyPlayer.get(), MyApple);
        std::cout << "Player thrown spoon at the enemy!" << std::endl;
        std::cout << "The enemy has left " << MyEnemy->ActorHealth.ChangeHealth(&AppleWeapon) << "HP" << std::endl;
    }

    return 0;
}
