//
// Created by Виктор Борисов on 17/12/22.
//

#ifndef UNTITLED7_GAME_COMPONENTS_H
#define UNTITLED7_GAME_COMPONENTS_H

#include <iostream>
#include <vector>
#include <cmath>


/////////////////////////////////////////////////////////////////////////////////////////
//// FActorClass
/////////////////////////////////////////////////////////////////////////////////////////
enum class FActorClass
{
    Player,
    Enemy
};

/////////////////////////////////////////////////////////////////////////////////////////
//// Class Weapon
/////////////////////////////////////////////////////////////////////////////////////////
class Weapon
{
protected:
    float Damage{};
public:
    Weapon() = default;
    ~Weapon() = default;

    virtual void SetDamage(float NewDamage)            { Damage = NewDamage; }
    virtual float GetDamage()                          { return Damage; }
};

/////////////////////////////////////////////////////////////////////////////////////////
//// Class Health
/////////////////////////////////////////////////////////////////////////////////////////
class Health
{
protected:
    float CurrentHealth{100};
public:
    Health() = default;
    ~Health() = default;

    /* work with Actor's health */
    void SetHealth(float NewHealth)                 { CurrentHealth = NewHealth; }
    float GetHealth() const                         { return CurrentHealth; }
    float ChangeHealth(Weapon* UsedWeapon)
    {
        CurrentHealth -= UsedWeapon->GetDamage();
        return CurrentHealth;
    };

};


/////////////////////////////////////////////////////////////////////////////////////////
//// Composite of wearable items
/////////////////////////////////////////////////////////////////////////////////////////
class WearableItems
{
protected:
    float Weight = 0.0f;
    float Price = 0.0f;
public:

    virtual float GetPrice()                        { return Price; }
    virtual void SetPrice(float AddPrice)           { Price += AddPrice; }

    virtual float GetWeight()                       { return Weight; }
    virtual void SetWeight(float AddWeight)         { Weight += AddWeight; }

};


class Shootgun: public WearableItems, public Weapon
{
public:
    Shootgun(){
        Weight = 10.0f;
        Price = 40.0f;
        Damage = 20.f;
    };
    ~Shootgun() = default;
};


class Apple: public WearableItems
{
public:
    Apple(){
        Weight = 1.0f;
        Price = 2.0f;
    };
    ~Apple() = default;
};


class Spoon: public WearableItems
{
public:
    Spoon(){
        Weight = 0.5f;
        Price = 0.5f;
    };
    ~Spoon() = default;
};


class Bag: public WearableItems
{
protected:
    std::vector<WearableItems*> Slots;
public:
    Bag()
    {
        Weight = 0.5f;
        Price = 0.0f;
    };
    ~Bag() = default;
    void AppendNewSlot(WearableItems* NewSlot)
    {
        Slots.push_back(NewSlot);
        SetPrice(NewSlot->GetPrice());
        SetWeight(NewSlot->GetWeight());
    }

    std::vector<WearableItems*> GetSlots()           {return Slots;}

};


/////////////////////////////////////////////////////////////////////////////////////////
//// GameVector
/////////////////////////////////////////////////////////////////////////////////////////
class GameVector
{
protected:
    float x = 0.0f;
    float y = 0.0f;
    float z = 0.0f;

public:
    explicit GameVector()= default;
    GameVector(float _x, float _y, float _z);

    ~GameVector()= default;


    void SetCoordinates(GameVector& TargetVector);
    void SetCoordinates(float _x, float _y, float _z);

    float GetLength() const;

    friend GameVector operator+(const std::shared_ptr<GameVector>& FirstVec, const std::shared_ptr<GameVector>& SecVec)
    {
        auto OutVector = GameVector(GameVector(FirstVec->x + SecVec->x,
                                               FirstVec->y + SecVec->y,
                                               FirstVec->z + SecVec->z));
        return OutVector;
    };

    friend GameVector operator-(const std::shared_ptr<GameVector>& FirstVec, const std::shared_ptr<GameVector>& SecVec)
    {
        GameVector OutVector = GameVector(FirstVec->x - SecVec->x,
                                          FirstVec->y - SecVec->y,
                                          FirstVec->z - SecVec->z);
        return OutVector;
    };

    friend std::ostream& operator<<(std::ostream& out, const GameVector& TargetVec)
    {
        out << "x=" << TargetVec.x << "; y=" << TargetVec.y << "; z=" << TargetVec.z;
        return out;
    }

};


/////////////////////////////////////////////////////////////////////////////////////////
//// Actor
/////////////////////////////////////////////////////////////////////////////////////////
class Actor
{
protected:
    std::shared_ptr<GameVector> ActorLocation = std::make_shared<GameVector>(GameVector());
    std::shared_ptr<GameVector> ActorRotation = std::make_shared<GameVector>(GameVector());
    float Opacity = 1;
    float PhysStrength = 10;
public:
    explicit Actor() = default;
    ~Actor() = default;

    Health ActorHealth = Health();


    /* work with Actor's Location */
    void SetActorLocation(GameVector* TargetVector);
    void SetActorLocation(float x, float y, float z);
    std::shared_ptr<GameVector> GetActorLocation() const;


    /* work with Actor's Rotation */
    void SetActorRotation(GameVector* TargetVector);
    void SetActorRotation(float x, float y, float z);
    std::shared_ptr<GameVector> GetActorRotation() const;

    /* work with Actor's physical strength */
    void SetActorPhysStrength(float NewPhysStrength);
    float GetActorPhysStrength() const;


    /* work with Actor's Opacity */
    void SetActorOpacity(float TargetOpacity);
    float GetActorOpacity() const;


};


/////////////////////////////////////////////////////////////////////////////////////////
//// Class Adapter for Weapon and WearableItems
/////////////////////////////////////////////////////////////////////////////////////////
class WearableItemsLikeWeapon
{

public:
    WearableItemsLikeWeapon() = default;
    ~WearableItemsLikeWeapon() = default;

    static Weapon GetNewWeapon(Actor* OwnerActor, WearableItems* NeedItems)
    {
        Weapon CrazyWeapon = Weapon();
        CrazyWeapon.SetDamage(NeedItems->GetWeight() * OwnerActor->GetActorPhysStrength());
        return CrazyWeapon;
    }
};


/////////////////////////////////////////////////////////////////////////////////////////
//// Player
/////////////////////////////////////////////////////////////////////////////////////////
class Player: public Actor
{
protected:

public:
    Bag PlayerBag = Bag();

    explicit Player() = default;
    Player(GameVector TargetLocation, GameVector& TargetRotation, float TargetOpacity)
    {
        ActorLocation->SetCoordinates(TargetLocation);
        ActorRotation->SetCoordinates(TargetRotation);
        Opacity = TargetOpacity;
    }
    ~Player(){};

};


/////////////////////////////////////////////////////////////////////////////////////////
//// Enemy
/////////////////////////////////////////////////////////////////////////////////////////
class Enemy: public Actor
{
public:
    explicit Enemy() = default;
    Enemy(GameVector TargetLocation, GameVector& TargetRotation, float TargetOpacity)
    {
        ActorLocation->SetCoordinates(TargetLocation);
        ActorRotation->SetCoordinates(TargetRotation);
        Opacity = TargetOpacity;
    }
    ~Enemy() = default;
};


/////////////////////////////////////////////////////////////////////////////////////////
//// ActorFactory
/////////////////////////////////////////////////////////////////////////////////////////
class ActorFactory
{
public:
    static Actor* NewActor(FActorClass TargetClass, GameVector TargetLocation,
                           GameVector& TargetRotation, float TargetOpacity)
    {
        Actor* MyActor = nullptr;

        if (TargetClass == FActorClass::Player)
            MyActor = static_cast<Actor*>(new Player(TargetLocation,
                                                      TargetRotation,
                                                      TargetOpacity));
        else if (TargetClass == FActorClass::Enemy)
            MyActor = static_cast<Actor*>(new Enemy(TargetLocation,
                                                     TargetRotation,
                                                     TargetOpacity));

        return MyActor;
    };
};

#endif //UNTITLED7_GAME_COMPONENTS_H
