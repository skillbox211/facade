//
// Created by Виктор Борисов on 17/12/22.
//
#include "game_components.h"
/////////////////////////////////////////////////////////////////////////////////////////
//// GameVector
/////////////////////////////////////////////////////////////////////////////////////////

GameVector::GameVector(float _x, float _y, float _z){
    SetCoordinates(_x, _y, _z);
}

void GameVector::SetCoordinates(GameVector& TargetVector)
{
    SetCoordinates(TargetVector.x, TargetVector.y, TargetVector.z);
}

void GameVector::SetCoordinates(float _x, float _y, float _z)
{
    x = _x;
    y = _y;
    z = _z;
}

float GameVector::GetLength() const
{
    return sqrt(x*x + y*y + z*z);
}


/////////////////////////////////////////////////////////////////////////////////////////
//// Actor
/////////////////////////////////////////////////////////////////////////////////////////

/* work with Actor's Location */
void Actor::SetActorLocation(GameVector* TargetVector)
{
    ActorLocation = static_cast<const std::shared_ptr<GameVector>>(TargetVector);
}

void Actor::SetActorLocation(float x, float y, float z)
{
    ActorLocation->SetCoordinates(x, y, z);
}

std::shared_ptr<GameVector> Actor::GetActorLocation() const
{
    return ActorLocation;
}


/* work with Actor's Rotation */
void Actor::SetActorRotation(GameVector* TargetVector)
{
    ActorRotation = static_cast<const std::shared_ptr<GameVector>>(TargetVector);
}

void Actor::SetActorRotation(float x, float y, float z)
{
    ActorRotation->SetCoordinates(x, y, z);
}

std::shared_ptr<GameVector> Actor::GetActorRotation() const
{
    return ActorRotation;
}

/* work with Actor's physical strength */
void Actor::SetActorPhysStrength(float NewPhysStrength)
{
    PhysStrength = NewPhysStrength;
}
float Actor::GetActorPhysStrength() const
{
    return PhysStrength;
}


/* work with Actor's Opacity */
void Actor::SetActorOpacity(float TargetOpacity)
{
    Opacity = TargetOpacity;
}

float Actor::GetActorOpacity() const
{
    return Opacity;
}

